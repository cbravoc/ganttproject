require_dependency "ganttproject/application_controller"

module Ganttproject
  class ResourcesController < ApplicationController
    before_action :set_resource, only: [:show, :edit, :update, :destroy]
    before_action :get_project

    def get_project
      @project = Project.find(params[:project_id])
    end
    # GET /resources
    # GET /resources.json
    def index
      @resources = @project.resources.all
    end

    # GET /resources/1
    # GET /resources/1.json
    def show
      @resources = @project.resources.find(params[:id])
    end

    # GET /resources/new
    def new
      @resource = @project.resources.new
    end

    # GET /resources/1/edit
    def edit
    end


    # POST /resources
    def create
      @resource = @project.resources.new(params[:resource])

      if @resource.save
        redirect_to @resource, notice: 'Recurso creado satifactoriamente'
      else
        render :new
      end
    end

    # PATCH/PUT /resources/1
    def update
      if @resource.update(resource_params)
        redirect_to @resource, notice: 'Recurso actualizado satifactoriamente.'
      else
        render :edit
      end
    end

    # DELETE /resources/1
    def destroy
      @resource.destroy
      redirect_to resources_url, notice: 'Recurso eliminado satifactoriamente.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_resource
        @resource = @project.resources.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def resource_params
        params.require(:resource).permit(:name, :initials, :max_units, :cost, :project_id)
      end
  end
end
