module Ganttproject
  class Project < ActiveRecord::Base
    has_many :tasks , dependent: :destroy
    has_many :resources, dependent: :destroy
    has_many :links, dependent: :destroy


    attr_accessor :file
    after_save :process_project_file , if: :file


    def process_project_file
      t1 = Time.now
      filename= file.original_filename
      filepath=file.path
      project = MPXJ::Reader.read(filepath)
      project_id=self.id

      resource_id_hash = Hash.new
      task_id_hash = Hash.new

      logger.info("There are #{project.all_resources.size} resources in this project")
      project.all_resources.each do |resource|
        r=Resource.new
        r.project_id=project_id
        r.initials=resource.initials
        r.max_units=resource.max_units
        r.cost=resource.cost
        r.save

        resource_id_hash[resource.id]=r.id
      end


      logger.info("There are #{project.all_tasks.size} tasks in this project")
      project.child_tasks.each do |childTask|
        resursive_task(childTask,nil,project_id,task_id_hash)
      end


      logger.info("There are #{project.all_assignments.size} assignments in this project")
      project.all_assignments.each do |assignment|
        a=Assignment.new
        a.resource_id=resource_id_hash[assignment.resource.id] if assignment.resource
        a.task_id=task_id_hash[assignment.task.id] if assignment.task
        a.start=assignment.start
        a.finish=assignment.finish
        a.save
      end

      logger.info("Checking links...")
      project.all_tasks.each do |task|
        task.successors.each do |link|
          l=Link.new
          l.gtype = link.type
          l.source = task_id_hash[task.id]
          l.target = task_id_hash[project.get_task_by_unique_id(link.task_unique_id).id]
          l.project_id=project_id
          case link.type
            when "FS"
              l.gtype = 0
            when "SS"
              l.gtype = 1
            when "FF"
              l.gtype = 2
            when "SF"
              l.gtype = 3
            else
              l.gtype = 0
          end
          l.save
        end


      end

      t2 = Time.now
      logger.info("Time execution: #{t2-t1} segs")

      self.file =nil
      update file_name: filename

    end


    def resursive_task(task,task_id,project_id,task_id_hash)

      task.child_tasks.each do |childTask|
        t=Task.new
        t.name=childTask.name
        t.project_id=project_id
        t.parent_id=task_id
        t.start=childTask.start
        t.finish=childTask.finish
        t.duration=( Date.parse(childTask.finish.to_s) -Date.parse(childTask.start.to_s)).to_i
        t.progress==childTask.percent_complete
        t.save

        task_id_hash[childTask.id]=t.id
        resursive_task(childTask,t.id,project_id,task_id_hash) if childTask.child_tasks
      end

    end

  end
end
