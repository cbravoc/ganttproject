module Ganttproject
  class Task < ActiveRecord::Base
    has_ancestry
    has_many :assignments, dependent: :destroy
    has_many :resources, through: :assignments
    belongs_to :project, :foreign_key => "project_id"

    def from_params(params, id)
      self.name       = params["#{id}_text"].to_s
      self.start      = params["#{id}_start_date"].to_date
      self.finish      = params["#{id}_end_date"].to_date
      self.duration   = params["#{id}_duration"].to_i
      self.progress   = params["#{id}_progress"].to_f
      if params["#{id}_parent"].to_i>0
        self.parent_id     = params["#{id}_parent"].to_i
      end
      self.project_id=params[:id]

    end
  end
end
