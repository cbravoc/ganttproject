class CreateGanttprojectTasks < ActiveRecord::Migration
  def change
    create_table :ganttproject_tasks do |t|
      t.string :name
      t.datetime :start
      t.datetime :finish
      t.integer :duration
      t.float :progress
      t.integer :parent_id
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
