class CreateGanttprojectResources < ActiveRecord::Migration
  def change
    create_table :ganttproject_resources do |t|
      t.string :name
      t.string :initials
      t.float :max_units
      t.float :cost
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
