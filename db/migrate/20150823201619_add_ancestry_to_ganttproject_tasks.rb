class AddAncestryToGanttprojectTasks < ActiveRecord::Migration
  def self.up
    add_column :ganttproject_tasks, :ancestry, :string
    add_index :ganttproject_tasks, :ancestry
  end

  def self.down
    remove_column :ganttproject_tasks, :ancestry
    remove_index :ganttproject_tasks, :ancestry
  end
end
