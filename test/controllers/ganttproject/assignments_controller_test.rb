require 'test_helper'

module Ganttproject
  class AssignmentsControllerTest < ActionController::TestCase
    setup do
      @assignment = ganttproject_assignments(:one)
      @routes = Engine.routes
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:assignments)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create assignment" do
      assert_difference('Assignment.count') do
        post :create, assignment: { finish: @assignment.finish, resource_id: @assignment.resource_id, start: @assignment.start, task_id: @assignment.task_id }
      end

      assert_redirected_to assignment_path(assigns(:assignment))
    end

    test "should show assignment" do
      get :show, id: @assignment
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @assignment
      assert_response :success
    end

    test "should update assignment" do
      patch :update, id: @assignment, assignment: { finish: @assignment.finish, resource_id: @assignment.resource_id, start: @assignment.start, task_id: @assignment.task_id }
      assert_redirected_to assignment_path(assigns(:assignment))
    end

    test "should destroy assignment" do
      assert_difference('Assignment.count', -1) do
        delete :destroy, id: @assignment
      end

      assert_redirected_to assignments_path
    end
  end
end
